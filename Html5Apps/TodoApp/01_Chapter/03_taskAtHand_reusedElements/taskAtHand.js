"use strict";

$(document).ready(function () {
    window.app = new TaskAtHandApp();
    window.app.start();
});


function TaskAtHandApp() {
    // LocalStorage will be handled in AppStorage and "TKHTS" is our prefix for the localStorage key
    this.appStorage = new AppStorage("TKHTS");

    function setStatus(message) {
        $("#app>footer").text(message);
    }


    // if localStorage is having previous tasks load them and display
    this.loadTaskList = function () {
        var tasks = app.appStorage.getValue("taskList");
        if (tasks) {
            for (var i in tasks) {
                addTaskElement(tasks[i]);
            }
        }
    }

    // start the app
    this.start = function () {

        $("#new-task-name").keypress(function (e) {
            if (e.which == 13) // Enter key
            {
                addTask();
                saveTaskList();
                return false;
            }
        })
            .focus();
        this.loadTaskList();
        setStatus("WWW.TKHTS.COM");
    };
}

//save new task to the localStorage
var saveTaskList = function () {
    var tasks = [];

    //jquery each method iterates each element one by one
    $("#task-list .task span.task-name").each(function () {
        //put each taks in task array
        tasks.push($(this).text())
    });
    //put task array in the storage
    app.appStorage.setValue("taskList", tasks);
}


function addTask() {
    var taskName = $("#new-task-name").val();
    if (taskName) {
        addTaskElement(taskName);
        // Reset the text field
        $("#new-task-name").val("").focus();
    }
}




function addTaskElement(taskName) {
    var $task = $("#task-template .task").clone();
    $("span.task-name", $task).text(taskName);
    $("#task-list").append($task);
    $("button.delete", $task).click(function () {
        $task.remove();
        saveTaskList();
    });
    $("button.move-up", $task).click(function () {
        $task.insertBefore($task.prev());
        saveTaskList();
    });
    $("button.move-down", $task).click(function () {
        $task.insertAfter($task.next());
        saveTaskList();
    });
    $("button.edit", $task).click(function () {
        $("span.task-name", $task).hide()
            .siblings("input.task-name")
            .val($("span.task-name", $task).text())
            .show()
            .focus();
    });

    $("input.task-name", $task).change(function () {
        onChangeTaskName($(this));
        saveTaskList();
    }).blur(function () {
        $(this).hide().siblings("span.task-name").show();
    });

}

function onChangeTaskName($input) {
    $input.hide();
    var $span = $input.siblings("span.task-name");
    if ($input.val()) {
        $span.text($input.val());
    }
    $span.show();
}