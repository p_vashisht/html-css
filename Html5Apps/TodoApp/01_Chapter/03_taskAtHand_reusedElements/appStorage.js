/**
 * Created by Puneet on 13-10-2013.
 */


function AppStorage(appName) {

    // if appName is here then take appName. as  prefix and key will be suffix for the localStorage
    var prefix = (appName ? appName + "." : "");

    // if browser has localStorage feature
    this.localStorageSupported = (('localStorage' in window) && window['localStorage']);

    // set the task to localStorage
    this.setValue = function (key, val) {
        if (this.localStorageSupported){
            localStorage.setItem(prefix + key, JSON.stringify(val));
        }
        return this;
    };

    this.getValue = function (key) {
        if (this.localStorageSupported){
            return JSON.parse(localStorage.getItem(prefix + key));
        }
        else{
            return null;
        }
    };

    this.removeValue = function (key) {
        if (this.localStorageSupported){
            localStorage.removeItem(prefix + key);
        }
        return this;
    };

    this.removeAll = function () {
        var keys = this.getKeys();
        for (var i in keys) {
            this.remove(keys[i]);
        }
        return this;
    };

    this.getKeys = function (filter) {
        var keys = [];
        if (this.localStorageSupported) {
            for (var key in localStorage) {
                if (isAppKey(key)) {
                    // Remove the prefix from the key
                    if (prefix) {
                        // remove prefix from the key
                        key = key.slice(prefix.length);
                    }

                    // Check the filter
                    if (!filter || filter(key)) {
                        keys.push(key);
                    }
                }
            }
        }
        return keys;
    };


    // is this our key with our app prefix
    function isAppKey(key) {
        if (prefix) {
            return key.indexOf(prefix) === 0;
        }
        return true;
    };

    this.contains = function (key) {
        return this.get(key) !== null;
    };
}
