"use strict";


function MyApp(){
    var version = "v1.0";
    //set footer message
    function setStatus(message)
    {
        $("#app>footer").text(message);
    }
    this.start = function()
    {
        $("#app>header").append(" (version number : "+version+")");
        //call footer setStatus
        setStatus("I am Footer");
    };
}

// start jquery example
$(document).ready(function() {

    // add our app object to the window object
    window.app = new MyApp();

    //start our app
    window.app.start();
});
/*
var myApp = {
    version: "v1.0",
    setStatus: function(message)
    {
        $("#app>footer").text(message);
    },start: function()
    {
        $("#app>header").append(this.version);
        this.setStatus("ready");
    }
};

*/

