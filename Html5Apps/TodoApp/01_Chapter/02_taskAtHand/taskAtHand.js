"use strict";


function TaskAtHandApp(){
    var version = "v1.0";
    function setStatus(message)
    {
        $("#app>footer").text(message);
    }
    this.start = function()
    {
        $("#new-task-name").keypress(function(event) {
            if (event.which == 13) // Enter key
            {
                // user task will be added in this function
                addTask();
                return false;
            }
        })
            // after adding the task focus on the input box
            .focus();

        $("#app>header").append(" (version number : "+version+")");
        setStatus("I am Footer");
    };
}



$(document).ready(function() {
    window.app = new TaskAtHandApp();
    window.app.start();
});



function addTask()
{
    // take the value
    var taskName = $("#new-task-name").val();

    // if user has entered some task
    if (taskName)
    {
        // create dynamic <li> to display that task
        addTaskElement(taskName);
        // Reset the text field
        $("#new-task-name").val("").focus();
    }
}

function addTaskElement(taskName)
{
    /*
    var $task = $("<li></li>");
    $task.text(taskName);
    $("#task-list").append($task);
//    $("#task-list").append("<li>"+taskName+"</li>");
*/

    var $task = $("<li></li>");

    var $delete = $("<button class='delete'>X</button>");
    var $moveUp = jQuery("<button class='move-up'>^</button>");
    var $moveDown = $("<button class='move-up'>v</button>");

    $task.append($delete).append($moveUp).append($moveDown).append("<span class='task-name'>" + taskName + "</span>");
    $delete.click(function() {
        $task.remove();
    });
    $moveUp.click(function() {
        $task.insertBefore($task.prev());
    });
    $moveDown.click(function() {
        $task.insertAfter($task.next());
    });
    $("#task-list").append($task);
}

/*
var myApp = {
    version: "v1.0",
    setStatus: function(message)
    {
        $("#app>footer").text(message);
    },start: function()
    {
        $("#app>header").append(this.version);
        this.setStatus("ready");
    }
};

*/

