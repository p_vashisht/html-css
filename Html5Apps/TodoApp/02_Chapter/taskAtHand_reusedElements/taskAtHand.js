"use strict";

$(document).ready(function() {
    window.app = new TaskAtHandApp();
    window.app.start();


    $("#theme").change(function(){
        var theme = $("#theme option:selected").val();
        setTheme(theme);
    });

/*    $("#theme>option").change(function(){
        alert('hey');
        setTheme();
    });*/
});



function TaskAtHandApp() {
    this.appStorage = new AppStorage("TKHTS");

    function setStatus(message) {
        $("#app>footer").text(message);
    }

    this.loadTaskList = function(){
        var tasks = app.appStorage.getValue("taskList");
        if (tasks)
        {
            for (var i in tasks)
            {
                addTaskElement(tasks[i]);
            }
        }
    }

    this.start = function () {

        $("#new-task-name").keypress(function (e) {
            if (e.which == 13) // Enter key
            {
                addTask();
                saveTaskList();
                return false;
            }
        })
            .focus();
        this.loadTaskList();
        loadTheme();
        $("#theme").change(onChangeTheme());
        setStatus("WWW.TKHTS.COM");
    };
}



var saveTaskList = function()
    {
        var tasks = [];
        $("#task-list .task span.task-name").each(function() {
            tasks.push($(this).text())
        });
        app.appStorage.setValue("taskList", tasks);
    }


function addTask()
{
    var taskName = $("#new-task-name").val();
    if (taskName)
    {
        addTaskElement(taskName);
        // Reset the text field
        $("#new-task-name").val("").focus();
    }
}

function addTaskElement(taskName)
{
    var $task = $("#task-template .task").clone();
    $("span.task-name", $task).text(taskName);
    $("#task-list").append($task);
    $("button.delete", $task).click(function() {
        $task.remove();
        saveTaskList();
    });
    $("button.move-up", $task).click(function() {
        $task.insertBefore($task.prev());
        saveTaskList();
    });
    $("button.move-down", $task).click(function() {
        $task.insertAfter($task.next());
        saveTaskList();
    });
    $("button.edit", $task).click(function() {
        $("span.task-name", $task).hide()
            .siblings("input.task-name")
            .val($("span.task-name", $task).text())
            .show()
            .focus();
    });

    $("input.task-name", $task).change(function() {
        onChangeTaskName($(this));
        saveTaskList();
    }).blur(function() {
        $(this).hide().siblings("span.task-name").show();
    });

    $task.click(function() {
        onSelectTask($task);
    });

}


function onSelectTask($task)
{
    if ($task)
    {
        // Unselect other tasks
        $task.siblings(".selected").removeClass("selected");
        // Select this task
        $task.addClass("selected");
    }
}

function onChangeTaskName($input)
{
    $input.hide();
    var $span = $input.siblings("span.task-name");
    if ($input.val())
    {
        $span.text($input.val());
    }
    $span.show();
}

function onChangeTheme()
{
    var theme = $("#theme>option").filter(":selected").val();
    setTheme(theme);
}

function setTheme(theme)
{
    $("#theme-style").attr("href", "./../themes/" + theme + ".css");
    app.appStorage.setValue("theme", theme);
}

function loadTheme()
{
    var theme = app.appStorage.getValue("theme");
    if (theme)
    {
        setTheme(theme);
        $("#theme>option[value=" + theme + "]").attr("selected","selected");
    }
}

