package com.tkhts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


// @WebServlet("HelloServlet")
public class HelloServlet extends HttpServlet {
     
	public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		doGet(request, response);
	}
	
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        //content type must be set to text/event-stream
        response.setContentType("text/event-stream");   

        //encoding must be set to UTF-8
        response.setCharacterEncoding("UTF-8");

        PrintWriter pw = response.getWriter();
 
 
            pw.write("data: "+ System.currentTimeMillis() +"\n\n");
            

            /*try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        pw.close();
    }
}
