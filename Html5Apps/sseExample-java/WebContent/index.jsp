<!DOCTYPE HTML>
<html>
<head>
<title>TKHTS - SSE</title>
<style type="text/css">
#header {
	width: 65%;
	background-color: cornFlowerBlue;
	float: left;
	margin-bottom: 100px;
	padding: 10px;
}

#icon {
	width: 32%;
	background-color: darkSeaGreen;
	float: left;
	margin-bottom: 100px;
	text-align: right;
	padding: 10px;
}

h1 {
	margin: 0px;
	padding: 0px;
	color: mintCream;
}

#notification {
	text-align: center;
}

#sse {
	width: 40%;
	background: blanchdedAlmond;
	height: auto;
	font-size: 30px;
	font-weight: bold;
	border: 1px black solid;
	margin: 150px auto;
	padding: 20px 10px;
	clear: both;
	text-align: center;
}
</style>
</head>
<body>

	<div id="header">
		<h1>Techknow Heights Welcomes You</h1>
	</div>
	<div id="icon">
		<h1>
			<span id="notification">Notifications</span>
		</h1>
	</div>

	<div id="sse">
		Time sent by server: <span id="time"></span><br/>
		<button onclick="start()">Start</button>
	</div>
	<script type="text/javascript">
    var number_of_notifications = 0;
    function start() {
 
        var eventSource = new EventSource("HelloServlet");  // servlet
        
        eventSource.onopen = function(){
         	console.log("Connection established!"); 
        	number_of_notifications++;
        };
         
        eventSource.onmessage = function(event) {
/*             console.log(typeof event.data);
            document.getElementById('time').innerHTML = event.data; */ 
            document.getElementById('time').innerHTML = "<br/>"+new Date(parseInt(event.data)); 
            
            if(number_of_notifications>1){
	            document.getElementById("notification").innerHTML = "You have "+number_of_notifications+" notifications";
            }
            else{
	            document.getElementById("notification").innerHTML = "You have "+number_of_notifications+" notification";
            }

            };
    }
    </script>
    
    <p style="font-size: 2em;"><b><i>Time is sent by server and each time server sends information, notification number is increased by one</i></b></p>
</body>
</html>